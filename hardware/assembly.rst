﻿*************************
Procedimiento de ensamble
*************************

**Versión:** 1.7

Placa Microcontrolador
----------------------

Indicaciones previas:

.. image:: ../media/img/pcb_micro_real.png
   :align: right
   :scale: 50%

-  Las figuras que representan la placa están orientadas de manera que la parte superior es referenciada como el *Norte* de la placa, la parte izquierda es el *Oeste* de la placa, la parte inferior es el *Sur* de la placa, y la parte derecha es el *Este* de la placa. Nótese en la figura que las etiquetas agregadas indican el sentido de lectura de la placa.
-  Para alimentar la placa hay que armar un conector específico para conectarla a la fuente. Antes de alimentar la placa hay que corroborar que esté conectado con la polaridad correcta.
-  Al medir con un multímetro sobre los componentes, es importante que las puntas del instrumento se apoyen sobre los terminales del componente, y no sobre la pista en la que esté soldado o sobre un pad o el terminal de otro componente, conectado al componente que se quiere analizar.

+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 1   | Antes de conectar o soldar nada a la placa, medir continuidad entre cada par de pines adyacentes para corroborar que aquellos que no están vinculados en el esquemático no tengan continuidad.   |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 2   | Soldar la fuente la fuente de 5V y el interruptor de encendido.                                                                                                                                  |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+    
|          | -  C19 (10uF; 16V; SMD)                                                                                                                                                                          |
|          | -  C20 (100nF; 16V; SMD)…                                                                                                                                                                        |
|          | -  D3 (1N4007; SMD)…                                                                                                                                                                             |
|          | -  J4 (¿descripción?)…                                                                                                                                                                           |
|          | -  REG1 (LM7805)…                                                                                                                                                                                |
|          | -  S2 (Switch con retención)…                                                                                                                                                                    |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 3   | Corroborar la salida de tensión del regulador de 5V con multímetro alimentando el circuito con una fuente regulada en con un rango de tensión de 9v a 12v y una corriente limitada a 100mA (0,1A)|
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 4   | Soldar la fuente la fuente de 3.3V. Componentes a soldar:                                                                                                                                        |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|          | -  C18 (1uF; 16V; SMD) no tiene polaridad, por lo que va en el sentido de lectura de la placa.                                                                                                   |
|          | -  C21 (4.7uF; 16V; SMD)…                                                                                                                                                                        |
|          | -  D4 (LED Rojo SMD) tiene una marca (un punto) que debe ir para el Norte.                                                                                                                       |
|          | -  J4 (¿descripción?)…                                                                                                                                                                           |
|          | -  R14 (470ohm; 5%; SMD)…                                                                                                                                                                        |
|          | -  U2 (MCP1703)…                                                                                                                                                                                 |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 5   | Alimentar la placa y corroborar las salidas de tensión de los reguladores con multímetro (la fuente debe configurarse como en el Paso 3)                                                         |
|          | La tensión a la salida del regulador de 5V debe seguir siendo 5V ± 0.1V, mientras que la salida del regulador de 3.3V debe ser de 3.3V ± 0.1V.                                                   |
|          | Medir la tensión que cae sobre R14 para corroborar que la corriente que circula por ella es de VALOR_FALTANTE.                                                                                   |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 6   | Soldar la fuente la fuente de 3.3V. Componentes a soldar:                                                                                                                                        |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|          | -  C1 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C2 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C3 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C4 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C5 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C6 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C13 (10nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C14 (1uF; 6.3V; SMD)…                                                                                                                                                                         |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 7   | Alimentar la placa y corroborar que el consumo de corriente no supere los 150mA.                                                                                                                 |
|          | Para identificar posibles cortocircuitos desconectar la alimentación de la placa y medir capacitor por capacitor si tiene continuidad entre borne y borne.                                       |
|          | Corroborar que ningún capacitor haya quedado abierto: con la placa desconectada medir si hay VALOR_FALTANTE de capacidad;                                                                        |
|          | en caso de no haberla medir capacitor por capacitor para ver cuál no da el valor correcto, y repasar las soldaduras o cambiarlo.                                                                 |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 8   | Soldar los componentes necesarios para el funcionamiento del microcontrolador. Componentes a soldar:                                                                                             |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|          | -  C7 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C8 (10nF; 16V; SMD)…                                                                                                                                                                          |
|          | -  C9 (100nF; 16V; SMD)…                                                                                                                                                                         |
|          | -  C10 (18pF; 50V; SMD)…                                                                                                                                                                         |
|          | -  C11 (18pF; 50V; SMD)…                                                                                                                                                                         |
|          | -  R1 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  R2 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  R3 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  R4 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  R5 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  R6 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  R7 (10Kohm; 5%; SMD)…                                                                                                                                                                         |
|          | -  X1 (…)…                                                                                                                                                                                       |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 9   | Alimentar la placa y corroborar que el consumo de corriente no supere los 150mA.                                                                                                                 |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 10  | Soldar los LEDs (respetando sus colores) y el pulsador con sus respectivos resistores. Componentes a soldar:                                                                                     |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|          | -  D5 (LED Rojo SMD) tiene una marca (un punto) que debe ir para DIRECCION\_FALTANTE.                                                                                                            |
|          | -  D6 (LED Azul SMD) tiene una marca (un punto) que debe ir para DIRECCION\_FALTANTE.                                                                                                            |
|          | -  D7 (LED Amarillo SMD) tiene una marca (un punto) que debe ir para DIRECCION\_FALTANTE.                                                                                                        |
|          | -  D8 (LED Verde SMD) tiene una marca (un punto) que debe ir para DIRECCION\_FALTANTE.                                                                                                           |
|          | -  R15 (470ohm; 5%; SMD)…                                                                                                                                                                        |
|          | -  R16 (470ohm; 5%; SMD)…                                                                                                                                                                        |
|          | -  R17 (470ohm; 5%; SMD)…                                                                                                                                                                        |
|          | -  R19 (10Kohm; 5%; SMD)…                                                                                                                                                                        |
|          | -  S3 (…)…                                                                                                                                                                                       |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 11  | Alimentar la placa y corroborar que el consumo de corriente no supere los 150mA.                                                                                                                 |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 12  | Soldar los conectores CN1 (para programar) y J3 (los de la UART0).                                                                                                                               |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 13  | Programar el microcontrolador con un programa que no emplee ningún periférico, y corroborar que el programador valide la escritura.                                                              |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 14  | Programar el microcontrolador con el programa TEST\_LEDS y corroborar que se prendan en orden los LEDs durante 10s. Medir tensión en R15, R16, R17 y R18                                         |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 15  | Programar el microcontrolador con el programa TEST\_PULSADOR y corroborar que cuando se presiona el pulsador se encienden los LEDs                                                               |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 16  | Soldar el resto de los componentes. ¿Qué componentes serían éstos?                                                                                                                               |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 17  | Cargar el programa TEST\_UARTS y corroborar el correcto funcionamiento de las dos UARTS (la del XBEE y la del conector módulo WiFi/Bluetooth).                                                   |
|          | El programa enciende los LEDs cuando se recibe un comando específico de encendido y los apaga cuando recibe un comando específico de apagado.                                                    |
+----------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
                                                                                                                                                                                                                                                               |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |                                                                                                                                                                                                                                                                                        


Placa distribuidora 
-------------------

    Indicaciones previas a todos los pasos:

-  Al medir con un multímetro sobre los componentes, es importante que
       las puntas del instrumento se apoyen sobre los terminales del
       componente, y no sobre la pista en la que esté soldado o sobre un
       pad o el terminal de otro componente, conectado al componente que
       se quiere analizar.

+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 1  | Soldar todos los componentes, se recomienda dejar todos los conectores al final.                                                                                                                     |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 2  | Conectar la alimentación de la placa con fuente (al igual que el punto 2 del PCB placa microcontrolador) y chequear que no haya cortos.                                                              |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 3  | Conectar la placa distribuidora con la placa cerebro.                                                                                                                                                |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 4  | Cargar el soft TEST\_MOTORES y chequear con osciloscopio la salida de ambos motores (salidas: PW1,PWM2) ambos PWM al 70% de ciclo de actividad durante 5 segundos y luego al 30% durante 5 segundos. |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 5  | Conectar los motores y corroborar que funcionen en ambos sentidos.                                                                                                                                   |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 6  | Armar el cableado de cada motor con su encoder (ver manual de miniPi) y conectarlos.                                                                                                                 |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 7  | Cargar el soft TEST\_ENCODERS y corroborar su funcionamiento.                                                                                                                                        |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 8  | Desconectar los motores/encoders.                                                                                                                                                                    |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 9  | Armar los cables y la placas de sensores de Línea (ver manual de miniPi) y conectarlos.                                                                                                              |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 10 | Cargar el soft TEST\_SENSORES\_LINEA y corroborar su funcionamiento.                                                                                                                                 |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 11 | Quitar los sensores de línea.                                                                                                                                                                        |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 12 | Armar los cables de sensores de ultrasonido (ver manual de miniPi) y conectarlos.                                                                                                                    |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Paso 13 | Cargar el soft TEST\_SENSORES\_US y corroborar su funcionamiento.                                                                                                                                    |
+---------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+



Armado de la plataforma robótica completa
-----------------------------------------

+--------+----------------------------+-----------------+--------+-----------------------------------------------------------------+
| Paso 1 | Programming Console Icon \ | iOS 7 Iconset \ | Icons8 | Cargar el soft TEST\_PLATAFORMA y corroborar su funcionamiento. |
+========+============================+=================+========+=================================================================+
|        |                            |                 |        |                                                                 |
+--------+----------------------------+-----------------+--------+-----------------------------------------------------------------+

.. image:: ../media/img/minipi_explosionada.jpeg