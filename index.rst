.. MiniPi documentation master file, created by
   sphinx-quickstart on Sat Apr 11 19:58:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

***************
Proyecto MiniPi
***************

.. image:: media/img/image3.png
   :align: right
   :scale: 50 %


**MiniPi** es una mini plataforma robótica multipropósito, de construcción mecánica sencilla y hardware electrónico replicable, de bajo costo, que permite implementar tareas de robótica colaborativa.


Objetivos
---------

Se propone el desarrollo de un robot con arquitectura de control distribuida, para que actúe en un ambiente parcialmente estructurado donde, eventualmente, se distribuirán marcas en el terreno (artificiales o naturales), al efecto de colaborar con su localización. Estará dotado de sensores ultrasónicos, ópticos y mecánicos.
 
Como objetivo intermedio se propone el desarrollo de un segundo robot móvil, que será utilizado como socio, para la implementación de las estrategias colaborativas, utilizando las lecciones aprendidas durante el desarrollo de robots anteriores.
 
Este proyecto tendrá en consideración la utilización de componentes fácilmente adquiribles en el mercado local, que permita su adaptación a distintos problemas y que faciliten su transferencia al medio.

La finalidad del presente proyecto es, además de la concreción del “objetivo central”, generar experiencia en robótica colaborativa, por parte de todos los integrantes del grupo. Generar nuevas técnicas y herramientas y, al dar soluciones concretas a problemas de ingeniería, crear nuevo conocimiento y difusión mediante comunicaciones a congresos científicos, revistas y actividad de extensión.


.. toctree::
   :maxdepth: 2
   :caption: Introducicción

    Intro <intro/intro.rst>
    Installation


.. toctree::
   :maxdepth: 2
   :caption: Software

    Firmware <https://minipi.readthedocs.io/projects/Firmware/es/latest/>


.. toctree::
   :maxdepth: 2
   :caption: Hardware
   
    Hardware <hardware/hardware.rst>
    Assembly <hardware/assembly.rst>


.. toctree::
   :maxdepth: 2
   :caption: Publications   


.. toctree::
   :maxdepth: 2
   :caption: Additional Info
   
    Troubleshooting
    Support
    FAQ
    Licence



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
